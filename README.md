## XSL2KML - konwertuje dane z arkusza Excel do map KML

Mały skrypt do generowania mapek Google Earth z danych plików excela. Plik _xls_ musi mieć przynajmniej 4 kolumny:
* SITE4G_NAME
* ADDRESS
* NUMLATITUDE
* NUMLOGTITUDE

### Sposób użycia:
```
# pokaż help
python csv2kml.py --help
# procesuj tabelę nodeB na mapki KML
python csv2kml.py plik_xls_1 plik_xls_2 plik_xls_n [-c kolumna]
```
Wynikiem powinny być pliki _*.kmz*, które można odtworzyć popularnym programem Google Earth ([*.deb](https://dl.google.com/dl/earth/client/current/google-earth-stable_current_amd64.deb)).


### Zależności
Skrypt działa w oparciu o dodatkowe paczki, które trzeba wcześniej zainstalować:
* pip install lxml pykml xlrd pandas
    * lxml może wymagać dodatkowych bibliotek w celu kompilacji paczki
* lub pip install -r requirements.txt
    * lxml może wymagać dodatkowych bibliotek w celu kompilacji paczki
* lub ze strony: http://www.lfd.uci.edu/~gohlke/pythonlibs/ (wygodniejsza forma dla użytkowników windowsa)