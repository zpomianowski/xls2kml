#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import csv
import zipfile
import argparse
from argparse import RawTextHelpFormatter
import traceback
import pandas as pd
from lxml import etree
from pykml.factory import KML_ElementMaker as KML


class KMLBuilder():
    def __init__(self, path, column=None):
        self.ltedata = []
        self.path = path
        column = column if column is None else column.upper()

        df = pd.read_excel(open(path,'rb'), sheet_name=0)
        df.columns = map(lambda x: x.upper(), df.columns)
        
        dfs = [df]
        if column:
            dfs = [x for _,  x in df.groupby(df[column.upper()])]

        for _d in dfs:
            kmlobj = KML.kml()
            self.main = KML.Folder(self.path[:-3])
            kmlobj.append(self.main)

            # styles
            self.main.append(KML.Style(
                KML.IconStyle(
                    KML.scale(1.5),
                    KML.Icon(
                        KML.href("tower.png"),
                    ),
                    KML.hotSpot(x="0.5", y="0.2",
                                xunits="fraction", yunits="fraction"),
                ),
                id="bts"))

            self.main.append(self.map(_d))

            outputfile = (
                '%s_%s_%s.kmz' % (self.path[:-4], column, str(_d[column].values[0]))
                if column else '%s.kmz' % self.path[:-4])
            with zipfile.ZipFile(outputfile, 'w',
                             zipfile.ZIP_DEFLATED) as myzip:
                myzip.writestr('%s.kml' % self.path[:-4], etree.tostring(
                    kmlobj, encoding="utf-8"))
                myzip.write('tower.png', 'tower.png')
                myzip.close()

      

    def map(self, df, name="sites"):
        fld = KML.Folder(KML.name("%s [%d]" % (name, len(self.ltedata))))

        for bts in df.itertuples():
            # import ipdb; ipdb.set_trace()
            pl = KML.Placemark(
                KML.name(''),
                KML.description(
                    # opis w chmurce
                    u"<h1>{name}</h1><br/>{desc}<br/><br/>".format(
                        name=bts.SITE4G_NAME,
                        desc=bts.ADDRESS),
                    u"<br/>".join(
                        ["<b>%s</b>: %s" % (k, v) for (k, v) in vars(bts).items()
                         if k not in ['Index', 'SITE', 'NAME', 'SITE4G_NAME']])),
                KML.styleUrl('#bts'),
                KML.Point(KML.coordinates("%s, %s" % (
                    # może się zdarzyć, że notacja liczb jest europejska
                    # zamiast amerykańskiej
                    bts.NUMLONGITUDE,
                    bts.NUMLATITUDE))))
            fld.append(pl)
        return fld

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description=(
        "Converts xls data into kml maps. "
        "Mandatory columns:\n\tSITE4G_NAME\n\tNAME\n\tNUMLONGITUDE\n\tNUMLATITUDE"),
        formatter_class=RawTextHelpFormatter)
    parser.add_argument(
        'xls_file', type=str, nargs='+',
        help='path(s) to xls files')
    parser.add_argument(
        '-c',
        help='split data into separate files by values in this column')
    args = parser.parse_args()
    print args

    for f in args.xls_file:
        try:
            KMLBuilder(f, args.c)
        except:
            traceback.print_exc(file=sys.stdout)
