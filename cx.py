#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
from cx_Freeze import setup, Executable

VERSION = '0.0.1'

# Dependencies are automatically detected, but it might need fine tuning.
build_exe_options = {
    "excludes": ["tkinter"],
    "optimize": 2}
    # "append_script_to_exe": True,
    # "include_in_shared_zip": True}

shortcut_table = [
    ("DesktopShortcut",                    # Shortcut
     "DesktopFolder",                      # Directory_
     "xls2kml",                            # Name
     "TARGETDIR",                          # Component_
     "[TARGETDIR]\\xls2kml.exe",            # Target
     None,                                 # Arguments
     None,                                 # Description
     None,                                 # Hotkey
     None,                                 # Icon
     None,                                 # IconIndex
     None,                                 # ShowCmd
     "TARGETDIR"                           # WkDir
     )]

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None
options = {
    "build_exe": build_exe_options,
    }

if sys.platform == "win32":
    options = {
        "build_exe": build_exe_options,
        "bdist_msi": {
            "initial_target_dir": "c:\\xls2kml",
            "data": {"Shortcut": shortcut_table}
            }
        }

setup(name="xls2kml",
      version=VERSION,
      description="## XSL2KML - konwertuje dane z arkusza Excel do map KML",
      options=options,
      executables=[Executable("xls2kml.py", targetName="xls2kml.exe")])