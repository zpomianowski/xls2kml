from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need fine tuning.
# setup not tested!!
# no warranty!!
build_exe_options = {
    "packages": ["lxml", "pykml", "collections", "sys", "os"],
    "excludes": ["tkinter"],
    "zip_exclude_packages": ["tkinter"],
    "zip_include_packages": "*",
    "include_files": ["tower.png"],
    "add_to_path": True
    # "copy_dependent_files": True,
    # "create_shared_zip": True
    }


setup(name="csv2kml",
      version="0.2",
      description="Converts csv sites to kml maps",
      options={
          "build_exe": build_exe_options,
          "bdist_msi": {
              "initial_target_dir": "c:\LAB\csv2kml"
          },
      },
      executables=[Executable("csv2kml.py")])
